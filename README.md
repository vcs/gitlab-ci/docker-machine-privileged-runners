__This repository contains the necessary objects required to start gitlab-runner pods in Openshift that make use of Docker-machine to run jobs on using Nova service from https://openstack.cern.ch.__

# Index
- [Index](#index)
- [Resources](#resources)
  - [Openstack project resources](#openstack-project-resources)
  - [Ceph resources (S3 Object Storage)](#ceph-resources-s3-object-storage)
  - [Gitlab project dependencies](#gitlab-project-dependencies)
- [Deployment](#deployment)
  - [Create the Keypair for the clusters](#create-the-keypair-for-the-clusters)
  - [Upload a CoreOS image to the Openstack tenant](#upload-a-coreos-image-to-the-openstack-tenant)
  - [Create the runner deployment config](#create-the-runner-deployment-config)
  - [Increasing the number of runners](#increasing-the-number-of-runners)
- [Updating a runner](#updating-a-runner)


# Resources
Included here are all the required resources for this deployment
## Openstack project resources
If necessary, an increase of project quota can be requested by opening a SNOW ticket.

The required resources that will be used are:

* __Openstack Project__: `IT Gitlab CI`
* __Service account__: `runnersciadmin`
* __Openstack administration egroup__: `runnersci-openstack-admins` (admin egroup: `it-service-vcs`)

In order to be able to follow the complete procedure, the service account `runnersciadmin` needs to be member of `runnersci-openstack-admins` egroup, which will be the administrator egroup for the Openstack Project.

In order to manage the Openstack project with your own account you need to be part of the `runnersci-openstack-admins` egroup as well.

In order to manage the Openshift project with your account you need to be part of the `it-service-vcs` egroup,
which has admin privileges on the `gitlab` openshift project.

`runnersciadmin` credentials are included in he `gitlab-secrets` ConfigMap in both `gitlab` projects on Openshift, `prod` and `dev`.
To retrieve them please use the following command while logged in `openshift.cern.ch` or `openshift-dev.cern.ch`:
```bash
oc get -n gitlab configmap gitlab-secrets -o yaml | grep runners-pwd
```


## Ceph resources (S3 Object Storage)
Some runners have cache enabled, in order to ask for a new instance from the Ceph central service, please open a [SNOW ticket](https://cern.service-now.com/service-portal/report-ticket.do?name=general-help-rqf&fe=service-desk).

Here you can find the request that has been fulfilled: [RQF0989367](https://cern.service-now.com/service-portal/view-request.do?n=RQF0989367)

The provided credentials are needed in the following steps.

The endpoint must have a bucket named `swarmcache` or `swarmcachedev` as it will be used on the configuration. In order to make it easier, use either https://s3browser.com/ or the `bucket_creation.py` on this project as it follows:
```bash
# Requirements
pip install boto
# Create the bucket
python bucket_creation.py <access_key> <secret_key> <s3_server>
```

## Gitlab project dependencies

* <https://gitlab.cern.ch/vcs/gitlab-runner-docker-machine>: we need to override the docker-machine client of the gitlab-runner to include metadata options (As of March 2019) and add registration scripts.
* <https://gitlab.cern.ch/vcs/docker-machine-privileged-runners-cleanup>: we need to clean up the Openstack tenant in case any VM is not deleted.

# Deployment

Preparation tasks need to be done only once for all the runners. 
In order to run __all__ the commands, please connect to `lxplus-cloud` as `runnersciadmin`: `ssh runnersciadmin@lxplus-cloud`. 

To be able to work on the Openstack project an env variable needs to be changed:
  
```bash
export OS_PROJECT_NAME='IT Gitlab CI'
```

## Create the Keypair for the clusters
After logging in as `runnersciadmin`, if the keypair is inexistant, please create a new one:

```bash
#In order to check if it already exists:
openstack keypair list 
#To create the Key pair:
openstack keypair create privileged-runner > ~/.ssh/docker-machine.pem
#Give rights to the owner for R/W:
chmod 600 ~/.ssh/docker-machine.pem
```

This keypair is stored in the `gitlab-secrets` configmap with the key `ssh_docker_machine_rsa_key`.

## Upload a CoreOS image to the Openstack tenant

In order to run jobs on spawned VMs, we need an specific image on the tenant: https://coreos.com/os/docs/latest/booting-on-openstack.html

Please follow http://clouddocs.web.cern.ch/clouddocs/details/user_supplied_images.html for instructions on how to upload the CoreOS image.

The uploaded image has to be named `coreos`.

## Create the runner deployment config

These steps need to be done for each cluster, all of them using the same public cluster template.

Run the corresponding CI stage

## Increasing the number of runners

Due to limitations with Docker-machine (see https://github.com/docker/machine/issues/3845#issuecomment-271935924) the concurrency per Gitlab-runner is restricted to 1.
If we need more runners, simply scale up the deployment, it will register a new runner and start picking new jobs.

# Updating a runner

If you want to update the runner configuration of an existing deployment, you should adapt the registration configuration if needed and update the deployment upstream image.